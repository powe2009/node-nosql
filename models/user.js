const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');

class User {
  constructor (name, email, id) {
    this.name = name;
    this.email = email;
    this.id = id;
  }

  static findOne(userId) {
    const db = getDb();
    return db.collection('users')
      .find({_id: new mongodb.ObjectId(userId)})
      .next()
      .then(user => {
        return user;
      })
      .catch(error => {
        console.log("TCL: User -> save -> error", error)
      })
  }

  static deleteOne(userId) {
    const db = getDb();
    let id = new mongodb.ObjectId(userId);
    
    return db.collection('users')
      .deleteOne({_id: id})
      .then (result => {
        return result;
      })
  }

  save() {
    const db = getDb();
    if (this.id) {
       return db.collection('users')
        .updateOne({_id: new mongodb.ObjectId(this.id)}, {$set: { name: this.name, email: this.email }})
        .then (result => {
          return result;
      })
    } else {
      return db.collection('users').insertOne({ name: this.name, email: this.email })
        .then (result => {
          return result;
      })
    }
  }

  static findAll() {
    const db = getDb();
    return db.collection('users')
      .find()
      .toArray()
      .then (users => {
        return users
      })
      .catch(error => {
        console.log("TCL: User -> findAll -> error", error)
      })
  }

}

module.exports = User;

