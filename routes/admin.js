const path = require('path');

const express = require('express');

const adminController = require('../controllers/admin');

const router = express.Router();

// /admin/products => GET
router.get('/products', adminController.getProducts);
// /admin/add-product => GET
router.get('/add-product', adminController.getAddProduct);
// /admin/add-product => POST
router.post('/add-product', adminController.postAddProduct);
router.get('/edit-product/:productId', adminController.getEditProduct);
router.post('/edit-product', adminController.postEditProduct);
router.post('/delete-product', adminController.postDeleteProduct);

// /admin/products => GET
router.get('/users', adminController.getUsers);
router.get('/add-user', adminController.addUser);
router.post('/add-user', adminController.postUser);
router.get('/users/:userId', adminController.getUser)
router.post('/edit-user', adminController.postUser);
router.post('/delete-user', adminController.deleteUser);

module.exports = router;
